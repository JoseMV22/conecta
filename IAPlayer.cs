using System;
using System.Collections.Generic;
using System.Timers;

namespace Conecta
{
    public class IAPlayer : Player
    {
        private static readonly int MS = 100;

        public IAPlayer(string name, string figureType) : base(name, "IA", figureType)
        {
        }

        public override Tuple<int, Board> play_turn()
        {
            
            Random rnd = new Random();
            List<int> availableActions = game.LegalAction();
            Node nodeSelection =  null;
            Node nodeParent = new Node(null, game.CopyBoard(), id, -1);

            long mSecondsOnStart = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond, diferencia = 0, mSecondsNow = 0;
            int indexMayor = 0;

            do
            {

                nodeSelection = Selection(nodeParent);
                nodeSelection = Expansion(nodeSelection);
                BackPropagation(nodeSelection, Simulation(nodeSelection));

                mSecondsNow = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                diferencia = mSecondsNow - mSecondsOnStart;

            } while(diferencia < MS);

            for (int i = 0; i < nodeParent.children.Count; i++)
            {
                if (nodeParent.children[i].reward > nodeParent.children[indexMayor].reward)
                {
                    indexMayor = i;
                }
            }

            Console.WriteLine("Turn of " + name + " with the figure " + figureType + ". ");
            return game.DoAction(id, nodeParent.children[indexMayor].nextThrow);
        }

        public Node Selection(Node nodo)
        {
            Node election = null;
            bool choiceOne = false;
            
            if (nodo.children.Count != 0)
            {
                while(!choiceOne){
                    
                    for (int i = 0; i < nodo.children.Count && !choiceOne; i++)
                    {
                        if (nodo.children[i].visits == 0)
                        {
                            election = nodo.children[i];
                            choiceOne = true;
                        }
                    }

                    if (!choiceOne)
                    {
                        election = calculoUCB1(nodo.children);
                        choiceOne = true;
                    }
                }
            }
            else
            {
                election = nodo;
            }

            return election;

        }

        private Node calculoUCB1(List<Node> nodesChildren)
        {
            double[] calculationsVCB1 = new double[nodesChildren.Count];
            double calculation;
            int higherIndex = 0;

            for (int i = 0; i < nodesChildren.Count; i++)
            {
                if (nodesChildren[i].parent == null)
                {
                    calculation = ((double) (nodesChildren[i].visits / nodesChildren[i].reward)) +
                                  1.5 * Math.Sqrt(Math.Log(nodesChildren[i].visits) / nodesChildren[i].visits);
                }
                else
                {
                    calculation = ((double) (nodesChildren[i].visits / nodesChildren[i].reward)) +
                                  1.5 * Math.Sqrt(Math.Log(nodesChildren[i].parent.visits) / nodesChildren[i].visits);
                }

                calculationsVCB1[i] = calculation;
            }

            for (int i = 0; i < calculationsVCB1.Length; i++)
            {
                if (calculationsVCB1[i] > calculationsVCB1[higherIndex])
                {
                    higherIndex = i;
                }
            }

            return nodesChildren[higherIndex];
        }

        public Node Expansion(Node node)
        {
            Random rnd = new Random();
            Tuple<int, Board> result = null;
            List<int> actions = game.LegalAction();
            Node election = null;
            Board newBoard = null;
            int nexplayer = game.NextPlayerId(id);

            if (actions.Count != 0)
            {
                for (int i = 0; i < actions.Count; i++)
                {
                    newBoard = game.CopyBoard();
                    result = game.SimAction(newBoard, id, actions[i] - 1);

                    node.children.Add(new Node(node, result.Item2, nexplayer, actions[i] - 1));
                }

                election = node.children[rnd.Next(node.children.Count)];
            }
            else
            {
                election = node;
                election.hoja = true;
            }

            return election;
        }

        public int Simulation(Node childrenSelection)
        {

            Random rnd = new Random();
            List<int> availableActions = childrenSelection.board.Available();
            Tuple<int, Board> result = new Tuple<int, Board>(0, childrenSelection.board);
            int playerPlay = childrenSelection.nextPlayer, action, resultSim;
            bool winner = false;

            while (availableActions.Count != 0 && !winner)
            {
                action = availableActions[rnd.Next(availableActions.Count)];
                result = game.SimAction(result.Item2, playerPlay,  action-1);
             
                if (result.Item1 == 1)
                {
                    winner = true;
                }
                else
                {
                    playerPlay = game.NextPlayerId(playerPlay);
                    availableActions = childrenSelection.board.Available();
                }

            }

            if (childrenSelection.nextPlayer == playerPlay && winner)
            {

                resultSim = 1;

            }else if (childrenSelection.nextPlayer != playerPlay && winner)
            {
                resultSim = -1;
            }
            else
            {
                resultSim = 0;
            }
            
            return resultSim;

        }

        public void BackPropagation(Node nodeSelection, int winner)
        {

            Node pathNodes = nodeSelection;

            while (pathNodes != null)
            {
                if (winner == 1)
                {
                    pathNodes.reward++;
                }
                else if (winner == 0)
                {
                    pathNodes.reward = 0.5;
                }

                pathNodes.visits++;
                pathNodes = pathNodes.parent;
            }
        }
    }
}