
using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace Conecta
{
    public class Game
    {
        public Board board;
        public List<Player> players { get; set; }
        public Player lastPlayer { get; set; }

        public Game(int[] size, int connect)
        {
            this.board = new Board(size[0],size[1],connect);
            this.players = new List<Player>();
        }

        public void AddPlayer(Player player)
        {
            this.players.Add(player);
            player.id = players.Count-1;
            player.game = this;
        }

        public void Start()
        {
            Tuple<int, Board> result = null;
            int index = 0;
            Player player = players[index];
            Console.WriteLine(board.ToString(players));

            do
            {
                result = player.play_turn();
                lastPlayer = player;

                if (result.Item1 == 1)
                {
                    Console.WriteLine(board.ToString(players));
                    Console.WriteLine(Environment.NewLine + "El ganador es " + player.name + " con id " + player.id + Environment.NewLine);
                }
                else
                {
                    index = NextPlayerId(index);
                    player = players[index];
                    Console.Clear();
                    Console.WriteLine(board.ToString(players));
                }

            } while (result.Item1 != -1 && result.Item1 != 1);

            if (result.Item1 == -1)
            {
                Console.WriteLine(board.ToString(players));
                Console.WriteLine(Environment.NewLine + "La partida acabado en empate, nadie gana!!" + Environment.NewLine);
            }
        }
        
        public Tuple<int, Board> DoAction(int playerId, int column)
        {
            Tuple<int, Board> result = board.PutToken(playerId, column), send;

            if (board.Available().Count == 0 && result.Item1 == 0)
            {
                send = new Tuple<int, Board>(-1, result.Item2);
            }
            else
            {
                send = result;
            }

            return send;
        }

        public Tuple<int, Board> SimAction(Board stateBoard, int playerId, int action)
        {
            Tuple<int, Board> result = stateBoard.SimToken(stateBoard, playerId, action), send;

            if (stateBoard.Available().Count == 0 && result.Item1 == 0)
            {
                send = new Tuple<int, Board>(-1, result.Item2);
            }
            else
            {
                send = result;
            }

            return send;

        }

        public List<int> LegalAction()
        {
            return board.Available();
        }

        public Tuple<Board, int> CurrentState()
        {
            return new Tuple<Board, int>(this.board, lastPlayer.id);
        } 

        public int NextPlayerId(int lastPlayerId)
        {
            if (lastPlayerId != players.Count-1)
            {
                return lastPlayerId +1;
            }
            else{
                return 0;
            }
        }

        public Board CopyBoard()
        {
            int[,] newBoard = new int[board.board.GetLength(0), board.board.GetLength(1)];

            for (int row = 0; row < board.board.GetLength(0); row++)
            {
                for (int col = 0; col < board.board.GetLength(1); col++)
                {
                    newBoard[row, col] = board.board[row, col];
                }
            }

            return new Board(newBoard, board.connect);

        } 

    }
}