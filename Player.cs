using System;

namespace Conecta
{
    public abstract class Player
    {
        
        public int id  { get; set; }
        public string name { get; set; }
        public string playerType { get; set; }
        public string figureType { get; set; }
        public Game game { get; set; }

        protected Player(string name, string playerType, string figureType)
        {
            id = 0;
            this.name = name;
            this.playerType = playerType;
            this.figureType = figureType;
            game = null;
        }

        public abstract Tuple<int, Board> play_turn();
    }
}