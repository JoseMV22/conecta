using System;
using System.Collections.Generic;

namespace Conecta
{
    public class RandomPlayer : Player
    {
        public RandomPlayer(string name, string figureType) : base(name, "CPU", figureType)
        {
        }

        public override Tuple<int, Board> play_turn()
        {
            Random rnd = new Random();
            List<int> availableActions = game.LegalAction();
            
            Console.Write("Turn of " + name + " with the figure " + figureType + ". ");
            int action = availableActions[rnd.Next(availableActions.Count)];
            return game.DoAction(id, action-1);
        }
    }
}