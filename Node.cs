using System;
using System.Collections.Generic;

namespace Conecta
{
    public class Node
    {

        public Node parent { get; set; }
        public List<Node> children { get; set; }
        public Board board { get; set; }
        public int nextPlayer { get; set; }
        public  int nextThrow { get; set; }
        public int visits { get; set; }
        public double reward { get; set; }
        public bool hoja { get; set; }

        public Node(Node parent, Board board, int nextPlayer, int nextThrow)
        {
            this.parent = parent;
            children = new List<Node>();
            this.board = board;
            this.nextPlayer = nextPlayer;
            this.nextThrow = nextThrow;
            visits = 0;
            reward = 0;
            hoja = false;
        }

        public void AddChildrenNode(Node node)
        {
            children.Add(node);
        }

    }
}