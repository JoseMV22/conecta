using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Conecta
{
    public class Board
    {
        public int[,] board { get; set; }
        public int connect { get; set; }

        public Board(int widht, int height, int connect)
        {
            board = CreateBoard(widht, height);
            this.connect = connect;
        }

        public Board(int[,] board, int connect)
        {
            this.board = board;
            this.connect = connect;
        }

        private int[,] CreateBoard(int widht, int height)
        {
            int[,] newBoard = new int[height+1,widht];

            for (int row = 0; row < newBoard.GetLength(0); row++)
            {
                for (int col = 0; col < newBoard.GetLength(1); col++)
                {
                    if (row == 0)
                    {
                        newBoard[row, col] = -2;
                    }
                    else
                    {
                        newBoard[row, col] = -1;
                    }
                }
            }

            return newBoard;
        }
        
        public Tuple<int, Board> PutToken(int playerId, int column)
        {
            Tuple<int, Board> result = null;
            bool introduced = false;

            for (int row = board.GetLength(0)-1; row > 0 && !introduced; row--)
            {
                if (board[row, column] == -1)
                {
                    board[row, column] = playerId;
                    introduced = true;
                    
                    if (WinningMove(row, column, playerId))
                    {
                        result = new Tuple<int, Board>(1, this);
                    }
                    else
                    {
                        result = new Tuple<int, Board>(0,this);
                    }
                }
            }
            
            return result;
        }

        public Tuple<int, Board> SimToken(Board stateBoard, int playerId, int action)
        {

            Tuple<int, Board> result = null;
            bool introduced = false;

            for (int row = stateBoard.board.GetLength(0) - 1; row > 0 && !introduced; row--)
            {
                if (stateBoard.board[row, action] == -1)
                {
                    stateBoard.board[row, action] = playerId;
                    introduced = true;

                    if (stateBoard.WinningMove(row, action, playerId))
                    {
                        result = new Tuple<int, Board>(1, stateBoard);
                    }
                    else
                    {
                        result = new Tuple<int, Board>(0, stateBoard);
                    }
                }
            }

            return result;
        }

        public List<int> Available()
        {
            List<int> columns = new List<int>();

            for (int col = 0; col < board.GetLength(1); col++)
            {
                if (board[1, col] == -1)
                {
                    columns.Add(col+1);
                }
            }
            
            return columns;
        }

        private bool WinningMove(int row, int col, int playerId)
        {
            bool win = false;
            
            if (CheckRow(row, playerId))
            {
                win = true;
            }
            else if (CheckCol(col, playerId))
            {
                win = true;
            }
            else if (CheckDiagonal(row, col, playerId))
            {
                win = true;
            }
            else if (CheckDiagonalInverse(row, col, playerId))
            {
                win = true;
            }

            return win;
        }

        private bool CheckCol(int col, int playerId)
        {
            bool check = false;
            int sum = 0;

            for (int row = board.GetLength(0)-1; row > 0 && !check; row--)
            {
                if (board[row,col] == playerId)
                {
                    sum++;
                }
                else
                {
                    sum = 0;
                }
                
                if (sum == this.connect)
                {
                    check = true;
                }
            }

            return check;
        }

        public bool CheckRow(int row, int playerId)
        {
            bool check = false;
            int sum = 0;

            for (int col = 0; col < board.GetLength(1) && !check; col++)
            {
                if (board[row,col] == playerId)
                {
                    sum++;
                }
                else
                {
                    sum = 0;
                }
                
                if (sum == this.connect)
                {
                    check = true;
                }
            }

            return check;
        }

        public bool CheckDiagonal(int row, int col, int playerId)
        {
            bool check = false;
            int newRow = row, newCol = col, sum = 0;

            if (row < col)
            {
                newRow -= row;
                newCol -= row;
            }
            else
            {
                newRow -= col;
                newCol -= col;
            }

            for (; newRow < board.GetLength(0) && newCol < board.GetLength(1) && !check; newRow++, newCol++)
            {
                if (board[newRow, newCol] == playerId)
                {
                    sum++;
                }
                else
                {
                    sum = 0;
                }
                
                if (sum == this.connect)
                {
                    check = true;
                }
            }
            
            return check;
        }
        
        private bool CheckDiagonalInverse(int row, int col, int playerId)
        {
            bool check = false;
            int newRow = row, newCol = col, difference = 0, sum = 0;

            newRow += col;
            newCol -= col;

            if (newRow > (board.GetLength(0) - 1))
            {
                difference = newRow - (board.GetLength(0) - 1);
                newRow -= difference;
                newCol += difference;
            }

            for (; newRow > 0 && newCol < board.GetLength(1) && !check; newRow--, newCol++)
            {
                if (board[newRow, newCol] == playerId)
                {
                    sum++;
                }
                else
                {
                    sum = 0;
                }
                
                if (sum == this.connect)
                {
                    check = true;
                }
            }

            return check;
        }

        public string MostrarTablero()
        {
            string mostrar = "";

            for (int row = 0; row < board.GetLength(0); row++)
            {
                for (int col = 0; col < board.GetLength(1); col++)
                {
                    /*if (board[row, col] == -2)
                    {
                        mostrar += (col+1) + "\t";
                    }
                    else
                    {*/
                    mostrar += board[row, col] + "\t";
                    //}
                }
                mostrar += Environment.NewLine;
            }

            return mostrar;
        }

        public string ToString(List<Player> players)
        {
            string toShowBoard = "\t\tConecta " + connect + Environment.NewLine + Environment.NewLine;

            for (int row = 0; row < board.GetLength(0); row++)
            {
                for (int col = 0; col < board.GetLength(1); col++)
                {
                    if (board[row, col] == -2)
                    {
                        toShowBoard += (col+1) + "\t";
                    }
                    else
                    {
                        if (board[row, col] == -1)
                        {
                            toShowBoard += " " + "\t";
                        }
                        else
                        {
                            foreach (var player in players)
                            {
                                if (player.id == board[row, col])
                                {
                                    toShowBoard += player.figureType + "\t";
                                }
                            }
                        }
                    }
                }
                toShowBoard += Environment.NewLine;
            }

            for (int i = 0; i < board.GetLength(0); i++)
            {
                toShowBoard += "------";
            }
            
            return toShowBoard;
        }

    }
}