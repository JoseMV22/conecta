using System;
using System.Collections.Generic;

namespace Conecta
{
    public class HumanPlayer : Player
    {
        public HumanPlayer(string name, string figureType) : base(name, "HUMAN", figureType)
        {
        }

        public override Tuple<int, Board> play_turn()
        {
            Tuple<int, Board> result = null;
            List<int> columns = game.LegalAction();
            bool next = false;
            string action;

            do
            {

                Console.Write("Turn of " + name + " with the figure " + figureType + ". ");
                Console.Write("You can enter a column number: ");
                action = Console.ReadLine();

                foreach (var column in columns)
                {
                    if ((int.Parse(action)) == column)
                    {
                        next = true;
                        break;
                    }
                }

                if (!next)
                {
                    Console.WriteLine(Environment.NewLine+"You can not move there!!"+Environment.NewLine);
                }
                else
                {
                    result = game.DoAction(id, int.Parse(action)-1);
                }
                
            } while (!next);

            return result;
        }
    }
}