﻿using System;
using System.Collections.Generic;

namespace Conecta
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] size = {6,6};
            Game MyGame = new Game(size, 4);
            
            //MyGame.AddPlayer(new IAPlayer("Jose", "X"));
            MyGame.AddPlayer(new HumanPlayer("Human", "O"));
            MyGame.AddPlayer(new IAPlayer("IA-Bot", "X"));
            MyGame.Start();
        }
    }
}